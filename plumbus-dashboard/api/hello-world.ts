import { VercelRequest, VercelResponse } from '@vercel/node';

export default (request: VercelRequest, response: VercelResponse) => {
    console.log(request.body);
    response.status(200).send(request.body);
};

